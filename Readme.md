# Aligner #

This is the Aligner plugin, a plugin for use in the Dungeon Helper framework.

This plugin automatically repositions client windows when running in Windowed (not Windowed Fullscreen) mode.

## Requirements ##
- dotnet-cli-zip - another dotnet tool:
```dotnet tool install --global dotnet-cli-zip --version 3.1.3```

## To make changes ##
- Increment the version in the project
- Add notes to the changelog for what was done.
- Push to a new branch, create a merge request.