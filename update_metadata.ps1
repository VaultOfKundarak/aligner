Param(
	$metadataFileName = "plugin.metadata"
)

$metadata = $null
$version=$env:version
$zipFile=$env:zipFile
$pluginName=$env:PLUGIN_NAME

if (-Not $(Test-Path $zipFile)) {
    Write-Output "Packaged zip file not found at $zipFile"
    exit 1;
}

if (-Not $(Test-Path $metadataFileName)) {
    Write-Output "Metadata file not found at $metadataFileName"
    exit 1;
}

Write-Output "Found package at $zipFile"

$metadata = Get-Content $metadataFileName | ConvertFrom-Json

if ($null -eq $metadata.CurrentVersion) {
    Write-Output "CurrentVersion in metadata was null"
    exit 1;
}

if ($version -eq $metadata.CurrentVersion) {
    Write-Output "Warning: CurrentVersion matched new version."
}

Write-Output "Setting CurrentVersion to $version"
$metadata.CurrentVersion = $version
$metadata.UpdateSource = "$METADATA_URL"
$metadata.ProductInfoUrl = "$PLUGIN_PAGE"
$metadata.PluginName = "$PLUGIN_NAME"
$found = $null
$archiveHash=$(CertUtil -hashfile $zipFile SHA512)[1] -replace " ",""
$url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/${pluginName}/${version}/${pluginName}_${version}.zip"

foreach ($av in $metadata.AllowedVersions) {
    if ($av.Version -eq $version) {
        Write-Output "Warning: metadata already had a matching version. Overwriting checksum."
        $av.Checksum=$archiveHash
        $av.Url=$url
        if ((Test-Path env:MIN_DH_FRAMEWORK) -eq $True) {
            Write-Output "Found minimum framework version $env:MIN_DH_FRAMEWORK"
            $av | add-member -Name "MinimumFrameworkVersion" -value $env:MIN_DH_FRAMEWORK -MemberType NoteProperty -Force -PassThru
        }
        $found=$av
    }
}

if ($null -eq $found) {
    $json="{ `"Version`": `"$version`", `"Url`": `"$url`", `"Checksum`":`"$archiveHash`", `"MinimumFrameworkVersion`":`"$env:MIN_DH_FRAMEWORK`" }"
    $newVersion = ConvertFrom-Json $json
    $metadata.AllowedVersions+=$newVersion
}

$newMetadata = ConvertTo-Json $metadata -Depth 4
Set-Content -Path $metadataFileName -value $newMetadata
