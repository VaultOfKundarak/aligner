Param (
    $projectFile
)

if (-Not $(Test-Path $projectFile)) {
    Write-Output "Project file not found at $projectFile"
    exit 1;
}

$folder=(Split-Path -Parent $projectFile)
$loc=(Get-Location)

try {
	$version=(Get-Content $projectFile) -match '<Version>' -replace '<Version>' -replace '</Version>' -replace ' '
	$version=$version.Trim()
}
catch {
    Write-Output "Error getting plugin name and version from project file"
	exit 1;
}

Set-Location $folder
dotnet publish -c Release -r win-x64 --no-self-contained -o $loc/builds/v$version/plugins/$PLUGIN_NAME | Tee-Object $loc/buildOutput.txt -Append

# need to remove stuff that shouldn't get packaged here. all these are included in Dungeon Helper, or 
# just shouldn't go at all (System.Drawing.Common, you bad boy.)
rm $loc/builds/v$version/plugins/$PLUGIN_NAME/System.Drawing.Common.dll
rm $loc/builds/v$version/plugins/$PLUGIN_NAME/*.pdb
rm $loc/builds/v$version/plugins/$PLUGIN_NAME/Microsoft.Windows.SDK.NET.dll
rm $loc/builds/v$version/plugins/$PLUGIN_NAME/VoK.Sdk.dll
rm $loc/builds/v$version/plugins/$PLUGIN_NAME/WinRT.Runtime.dll
rm $loc/builds/v$version/plugins/$PLUGIN_NAME/Newtonsoft.Json.dll
rm $loc/builds/v$version/plugins/$PLUGIN_NAME/log4net.dll

Set-Location $loc

if($LASTEXITCODE -eq 1)
{
    Write-Output "Build failed."
	exit 1
}

$env:version=$version