Param (
    $projectFile
)

if (-Not $(Test-Path $projectFile)) {
    Write-Output "Project file not found at $projectFile"
    exit 1;
}

$folder=(Split-Path -Parent $projectFile)
$loc=(Get-Location)
$inputFolder="$loc/builds/v$env:version"
$zipFile="$loc/builds/${env:PLUGIN_NAME}_${env:version}.zip"

if (Test-Path $zipFile) {
	Remove-Item $zipFile
}

z -c $inputFolder $zipFile

if($LASTEXITCODE -eq 1)
{
    Write-Output "Build failed."
	exit 1
}

$env:zipFile=$zipFile