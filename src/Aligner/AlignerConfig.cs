﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Aligner
{
    internal class AlignerConfig
    {
        public Dictionary<string, AccountConfig> AccountConfig = new Dictionary<string, AccountConfig>();
    }

    internal class AccountConfig
    {
        [JsonProperty("autoPosition")]
        public bool AutoRestore { get; set; }

        public bool AutoSave { get; set; }

        public int DesktopX { get; set; }

        public int DesktopY { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }
    }
}
