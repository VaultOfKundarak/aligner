﻿using IniParser;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using System.Windows.Forms;
using VoK.Sdk;
using VoK.Sdk.Ddo;
using VoK.Sdk.Plugins;
using VoK.Sdk.Windows;

namespace Aligner
{
    internal class Plugin : IDdoPlugin
    {
        public Guid PluginId => Guid.Parse("b40de30d-61d0-4619-ab80-4f2cd7aeda47");
        public GameId Game => GameId.DDO;
        public string PluginKey => "8473a831-dfc5-4645-8fda-48e65c80050f";
        public string Name => "Aligner";
        public string Description => "Automatically aligns windowed clients based on subscription";
        public string Author => "Morrikan";
        public Version Version => GetType().Assembly.GetName().Version;

        internal string Folder { get; set; }

        public static Plugin Instance { get; set; }

        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private PluginUI _ui;
        private IDdoGameDataProvider _gameDataProvider;
        private AlignerConfig _config;
        private string _accountHashKey;
        private IntPtr _ddoWindow;
        private IntPtr _locationChangeHook;
        private static WinApi.WinEventDelegate _hookDelegate;
        private bool _loadComplete = false;
        private bool _hooked = false;
        private bool _initialRelocation = false;

        public bool Enabled { get; private set; }

        public Plugin()
        {
            Instance = this;
            var myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var iniPath = Path.Combine(myDocs, "Dungeons and Dragons Online", "UserPreferences.ini");

            var iniFile = new FileIniDataParser().ReadFile(iniPath);

            try
            {
                var mode = iniFile["Display"]["ScreenMode"];
                Enabled = (mode == "Windowed");
            }
            catch (Exception ex)
            {
                _log.Error("Could not read user prefs file", ex);
                Enabled = false;
            }
        }

        public void Initialize(IDdoGameDataProvider gameDataProvider, string folder)
        {
            Folder = folder;
            _gameDataProvider = gameDataProvider;

            // accountHashKey is a unique-to-plugin hash of the subscription key, the unique identifier
            // of a playable account (or in SSG terms, a subscription). we'll use it for a key for our
            // settings collection

            // this will call back into RegisterHook() on the UI thread when appropriate.
            _ui = new PluginUI();

            _ = Task.Run(GetSubscriptionKeyHash);

        }

        private async Task GetSubscriptionKeyHash()
        {
            while (string.IsNullOrWhiteSpace(_accountHashKey))
            {
                await Task.Delay(500);
                _accountHashKey = _gameDataProvider.GetSubscriptionKeyHash(this);
            }

            LoadConfig();
            IngameUI.Instance.ReloadConfig();
            _loadComplete = true;
        }

        public void RegisterHook()
        {
            if (!Enabled) return;

            // only call this once. it's recursive and fairly cpu intensive
            _ddoWindow = WinApi.GetWindowHandle(_gameDataProvider.GameProcess.Id, "Turbine Device Class");

            _hookDelegate = new WinApi.WinEventDelegate(OnWindowMoved);
            // after the initial relocation, register our event listeners with windows
            _locationChangeHook = WinApi.SetWinEventHook(WinApi.WinHookEvents.EVENT_OBJECT_LOCATIONCHANGE,
                                                         WinApi.WinHookEvents.EVENT_OBJECT_LOCATIONCHANGE,
                                                         _ddoWindow, _hookDelegate, (uint)_gameDataProvider.GameProcess.Id, 0, 2u);
            _hooked = true;
            Relocate();
        }

        private string GetConfigPath()
        {
            return Path.Combine(Folder, "AlignerConfig.json");
        }

        private void LoadConfig()
        {
            var path = GetConfigPath();
            if (!File.Exists(path))
            {
                _config = new AlignerConfig();
                return;
            }

            var contents = File.ReadAllText(path);
            try
            {
                var config = JsonConvert.DeserializeObject<AlignerConfig>(contents);
                _config = config ?? new AlignerConfig();
            }
            catch
            {
                _config = new AlignerConfig();
            }
        }

        private void SaveConfig()
        {
            if (_config == null) return;

            var path = GetConfigPath();
            var content = JsonConvert.SerializeObject(_config);

            try
            {
                File.WriteAllText(path, content);
            }
            catch (Exception ex)
            {
                _log.Error("Error saving configuration", ex);
            }
        }

        public IPluginUI GetPluginUI()
        {
            return _ui;
        }

        public void Terminate()
        {
            WinApi.UnhookWindowsHookEx(_locationChangeHook);
        }

        public void Relocate(bool force = false)
        {
            if (!Enabled) return;

            _initialRelocation = true;

            if (string.IsNullOrWhiteSpace(_accountHashKey) || _config?.AccountConfig == null) return;
            if (!_config.AccountConfig.ContainsKey(_accountHashKey)) return;
            if (_config.AccountConfig[_accountHashKey] == null) return;
            var myConfig = _config.AccountConfig[_accountHashKey];

            var screens = Screen.AllScreens;
            var valid = false;
            var restore = force || myConfig.AutoRestore;
            foreach (var screen in screens)
                valid = valid || screen.Bounds.Contains(new Point(myConfig.DesktopX, myConfig.DesktopY));

            if (!valid || !restore) return;

            WinApi.GetWindowRect(_ddoWindow, out RECT location);

            var width = myConfig.Width;
            var height = myConfig.Height;

            var left = myConfig.DesktopX;
            var top = myConfig.DesktopY;

            _log.Info($"Attempting to Relocate DDO window to {left},{top} - {width},{height}");
            uint args = WinApi.SetWindowPosArgs.SWP_NOZORDER | WinApi.SetWindowPosArgs.SWP_SHOWWINDOW;
            WinApi.SetWindowPos(_ddoWindow, 0, left, top, width, height, args);
            // https://learn.microsoft.com/en-us/windows/win32/winmsg/wm-size
            // make calls into DDO to have it reset the UI to scale
            WinApi.SendMessage(_ddoWindow, WinApi.SendMessageIds.WM_ENTERSIZEMOVE, 0, 0);
            WinApi.SendMessage(_ddoWindow, WinApi.SendMessageIds.WM_EXITSIZEMOVE, 0, 0);
        }

        public bool AutoRestore
        {
            get
            {
                LoadConfig();
                if (string.IsNullOrWhiteSpace(_accountHashKey) || _config?.AccountConfig == null) return false;
                if (!_config.AccountConfig.ContainsKey(_accountHashKey)) return false;
                if (_config.AccountConfig[_accountHashKey] == null) return false;
                return _config.AccountConfig[_accountHashKey].AutoRestore;
            }
            set
            {
                SetupConfig();
                _config.AccountConfig[_accountHashKey].AutoRestore = value;
                SaveConfig();
            }
        }

        public bool AutoSave
        {
            get
            {
                LoadConfig();
                if (string.IsNullOrWhiteSpace(_accountHashKey) || _config?.AccountConfig == null) return true;
                if (!_config.AccountConfig.ContainsKey(_accountHashKey)) return true;
                if (_config.AccountConfig[_accountHashKey] == null) return true;
                return _config.AccountConfig[_accountHashKey].AutoSave;
            }
            set
            {
                SetupConfig();
                _config.AccountConfig[_accountHashKey].AutoSave = value;
                SaveConfig();

                if (value) SaveNow();
            }
        }

        public void SaveNow()
        {
            SetupConfig();

            WinApi.GetWindowRect(_ddoWindow, out RECT location);

            _config.AccountConfig[_accountHashKey].DesktopX = location.Left;
            _config.AccountConfig[_accountHashKey].DesktopY = location.Top;
            _config.AccountConfig[_accountHashKey].Width = location.Right - location.Left;
            _config.AccountConfig[_accountHashKey].Height = location.Bottom - location.Top;

            SaveConfig();
        }

        private void SetupConfig()
        {
            LoadConfig();
            if (_config == null)
            {
                _log.Error("Config was null when setting state.");
                return;
            }

            if (_config.AccountConfig == null)
                _config.AccountConfig = new Dictionary<string, AccountConfig>();

            if (!_config.AccountConfig.ContainsKey(_accountHashKey))
                _config.AccountConfig.Add(_accountHashKey, new AccountConfig());
        }

        public void OnWindowMoved(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            if (!_loadComplete || hwnd == IntPtr.Zero) return;

            if (hwnd == _ddoWindow)
            {
                WinApi.GetWindowRect(_ddoWindow, out RECT location);

                if (string.IsNullOrWhiteSpace(_accountHashKey) || _config?.AccountConfig == null) return;
                if (!_config.AccountConfig.ContainsKey(_accountHashKey)) return;
                if (_config.AccountConfig[_accountHashKey] == null) return;
                LoadConfig();
                var myConfig = _config.AccountConfig[_accountHashKey];
                if (!myConfig.AutoSave) return;

                myConfig.DesktopX = location.Left;
                myConfig.DesktopY = location.Top;
                myConfig.Width = location.Right - location.Left;
                myConfig.Height = location.Bottom - location.Top;
                _log.Info($"Aligner: saving new location {location.Left},{location.Top} : {myConfig.Width},{myConfig.Height}");

                SaveConfig();
            }
        }
    }
}
