﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoK.Sdk.Ddo;
using VoK.Sdk.Plugins;

namespace Aligner
{
    internal class PluginUI : IPluginUI
    {
        private IngameUI _ingameUi;

        public PluginUI()
        {
            _ingameUi = new IngameUI();
        }

        public float? FocusedOpacity => 1.0f;

        public bool EnabledInCharacterSelection => false;

        public Image ToolbarImage
        {
            get
            {
                return (Image)Properties.Resources.Icon.Clone();
            }
        }

        public object UserInterfaceForm => _ingameUi;

        public Tuple<int, int> MinSize => new(400, 200);
    }
}
