﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using VoK.Sdk.Windows;

namespace Aligner
{
    internal class WinApi
    {
        public delegate bool EnumWindowProc(IntPtr hwnd, IntPtr lParam);

        public delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool EnumWindows(EnumWindowProc enumProc, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int processId);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll")]
        public static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventDelegate lpfnWinEventProc, uint idProcess, uint idThread, uint dwFlags);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Not a Windows API. An intensive way of finding the primary windows handle of a process.
        /// </summary>
        public static IntPtr GetWindowHandle(int processId, string className)
        {
            IntPtr handle = IntPtr.Zero;

            // Cycle through all top-level windows
            EnumWindows(delegate (IntPtr hWnd, IntPtr lParam)
            {
                // Get PID of current window
                GetWindowThreadProcessId(hWnd, out int windowProcessId);

                if (processId == windowProcessId)
                {
                    // Get class name of current window
                    StringBuilder classNameBuilder = new StringBuilder(256);
                    GetClassName(hWnd, classNameBuilder, 256);

                    // Check if class name matches what we're looking for
                    if (classNameBuilder.ToString() == className)
                        handle = hWnd;
                }

                // return true so that we iterate through all windows
                return true;
            }, IntPtr.Zero);

            return handle;
        }

        public static class WinHookEvents
        {
            public const uint EVENT_OBJECT_LOCATIONCHANGE = 0x800B;
        }

        public static class SetWindowPosArgs
        {
            public const short SWP_NOMOVE = 0X2;
            public const short SWP_NOSIZE = 1;
            public const short SWP_NOZORDER = 0X4;
            public const int SWP_SHOWWINDOW = 0x0040;
        }

        public static class SendMessageIds
        {
            public const ushort WM_SIZE = 0x0005;
            public const ushort WM_SIZING = 0x0214;
            public const ushort WM_ENTERSIZEMOVE = 0x0231;
            public const ushort WM_EXITSIZEMOVE = 0x0232;
        }

        public static class SendMessageSizingParam
        {
            public const int WMSZ_LEFT = 1;
            public const int WMSZ_RIGHT = 2;
            public const int WMSZ_TOP = 3;
            public const int WMSZ_TOPLEFT = 4;
            public const int WMSZ_TOPRIGHT = 5;
            public const int WMSZ_BOTTOM = 6;
            public const int WMSZ_BOTTOMLEFT = 7;
            public const int WMSZ_BOTTOMRIGHT = 8;
        }
    }
}
