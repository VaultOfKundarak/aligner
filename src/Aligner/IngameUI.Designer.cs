﻿namespace Aligner
{
    partial class IngameUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IngameUI));
            lTitle = new System.Windows.Forms.Label();
            cbEnabled = new System.Windows.Forms.CheckBox();
            lError = new System.Windows.Forms.Label();
            cbAutoSave = new System.Windows.Forms.CheckBox();
            btnSaveNow = new System.Windows.Forms.Button();
            btnRestore = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // lTitle
            // 
            lTitle.AutoSize = true;
            lTitle.BackColor = System.Drawing.Color.Transparent;
            lTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            lTitle.ForeColor = System.Drawing.Color.Tan;
            lTitle.Location = new System.Drawing.Point(12, 9);
            lTitle.Name = "lTitle";
            lTitle.Size = new System.Drawing.Size(87, 26);
            lTitle.TabIndex = 0;
            lTitle.Text = "Aligner";
            // 
            // cbEnabled
            // 
            cbEnabled.AutoSize = true;
            cbEnabled.BackColor = System.Drawing.Color.Transparent;
            cbEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbEnabled.ForeColor = System.Drawing.Color.AntiqueWhite;
            cbEnabled.Location = new System.Drawing.Point(12, 51);
            cbEnabled.Name = "cbEnabled";
            cbEnabled.Size = new System.Drawing.Size(110, 21);
            cbEnabled.TabIndex = 1;
            cbEnabled.Text = "Auto Restore";
            cbEnabled.UseVisualStyleBackColor = false;
            cbEnabled.CheckedChanged += cbEnabled_CheckedChanged;
            // 
            // lError
            // 
            lError.AutoSize = true;
            lError.BackColor = System.Drawing.Color.Transparent;
            lError.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            lError.ForeColor = System.Drawing.Color.Tan;
            lError.Location = new System.Drawing.Point(12, 104);
            lError.Name = "lError";
            lError.Size = new System.Drawing.Size(110, 17);
            lError.TabIndex = 3;
            lError.Text = "[Set At Runtime]";
            lError.Visible = false;
            // 
            // cbAutoSave
            // 
            cbAutoSave.AutoSize = true;
            cbAutoSave.BackColor = System.Drawing.Color.Transparent;
            cbAutoSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            cbAutoSave.ForeColor = System.Drawing.Color.AntiqueWhite;
            cbAutoSave.Location = new System.Drawing.Point(182, 51);
            cbAutoSave.Name = "cbAutoSave";
            cbAutoSave.Size = new System.Drawing.Size(92, 21);
            cbAutoSave.TabIndex = 4;
            cbAutoSave.Text = "Auto Save";
            cbAutoSave.UseVisualStyleBackColor = false;
            cbAutoSave.CheckedChanged += cbAutoSave_CheckedChanged;
            // 
            // btnSaveNow
            // 
            btnSaveNow.Location = new System.Drawing.Point(182, 79);
            btnSaveNow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            btnSaveNow.Name = "btnSaveNow";
            btnSaveNow.Size = new System.Drawing.Size(94, 22);
            btnSaveNow.TabIndex = 5;
            btnSaveNow.Text = "Save Now";
            btnSaveNow.UseVisualStyleBackColor = true;
            btnSaveNow.Click += btnSaveNow_Click;
            // 
            // btnRestore
            // 
            btnRestore.Location = new System.Drawing.Point(12, 79);
            btnRestore.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            btnRestore.Name = "btnRestore";
            btnRestore.Size = new System.Drawing.Size(94, 22);
            btnRestore.TabIndex = 6;
            btnRestore.Text = "Restore Now";
            btnRestore.UseVisualStyleBackColor = true;
            btnRestore.Click += btnRestore_Click;
            // 
            // IngameUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackgroundImage = (System.Drawing.Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new System.Drawing.Size(391, 148);
            ControlBox = false;
            Controls.Add(btnRestore);
            Controls.Add(btnSaveNow);
            Controls.Add(cbAutoSave);
            Controls.Add(lError);
            Controls.Add(cbEnabled);
            Controls.Add(lTitle);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "IngameUI";
            ShowIcon = false;
            ShowInTaskbar = false;
            Text = "IngameUI";
            Load += IngameUI_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label lTitle;
        private System.Windows.Forms.CheckBox cbEnabled;
        private System.Windows.Forms.Label lError;
        private System.Windows.Forms.CheckBox cbAutoSave;
        private System.Windows.Forms.Button btnSaveNow;
        private System.Windows.Forms.Button btnRestore;
    }
}