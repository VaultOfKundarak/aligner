﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using VoK.Sdk;
using VoK.Sdk.Common;
using VoK.Sdk.Dat;
using VoK.Sdk.Ddo;
using VoK.Sdk.Ddo.Enums;
using VoK.Sdk.Enums;
using VoK.Sdk.Properties;

namespace Aligner
{
    public partial class IngameUI : Form
    {
        private bool _loading = false;

        public IngameUI()
        {
            InitializeComponent();

            Instance = this;
        }

        public static IngameUI Instance { get; private set; }

        public void ReloadConfig()
        {
            var a = new Action(() =>
            {
                _loading = true;
                cbEnabled.Checked = Plugin.Instance.AutoRestore;
                cbAutoSave.Checked = Plugin.Instance.AutoSave;
                _loading = false;

                // because windows is dumb, hooks must be registered in the UI thread
                Plugin.Instance.RegisterHook();

            });
            if (InvokeRequired) Invoke(a); else a();
        }

        private void cbEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (_loading) return;
            Plugin.Instance.AutoRestore = cbEnabled.Checked;
        }

        private void IngameUI_Load(object sender, EventArgs e)
        {
            cbEnabled.Enabled = Plugin.Instance.Enabled;

            if (!Plugin.Instance.Enabled)
            {
                lError.Text = "Aligner only works in Windowed mode";
                lError.Visible = true;
            }
        }

        private void cbAutoSave_CheckedChanged(object sender, EventArgs e)
        {
            btnSaveNow.Enabled = !cbAutoSave.Checked;

            if (_loading) return;
            Plugin.Instance.AutoSave = cbAutoSave.Checked;
        }

        private void btnSaveNow_Click(object sender, EventArgs e)
        {
            Plugin.Instance.SaveNow();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            Plugin.Instance.Relocate(true);
        }
    }
}
